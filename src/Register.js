import React, { useState } from "react";
import { Alert, Button, TextInput, View, StyleSheet } from "react-native";

const Register = ({ account, setAccount }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const registerHandler = () => {
    try {
      setAccount([
        ...account,
        {
          username: `${username}`,
          password: `${password}`,
        },
      ]);
    } catch (error) {
      console.log(error);
    } finally {
      setPassword("");
      setUsername("");
    }
  };

  return (
    <View style={styles.container}>
      <TextInput
        value={username}
        onChange={(e) => setUsername(e.target.value)}
        placeholder={"Username"}
        style={styles.input}
      />
      <TextInput
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        placeholder={"Password"}
        secureTextEntry={true}
        style={styles.input}
      />

      <Button
        title={"Register"}
        style={styles.input}
        onPress={registerHandler}
      />
    </View>
  );
};

export default Register;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    // backgroundColor: "#ecf0f1",
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 10,
  },
});
