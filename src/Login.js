import React, { useState } from "react";
import { Alert, Button, TextInput, View, StyleSheet } from "react-native";

const Login = ({ account }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  return (
    <View style={styles.container}>
      <TextInput
        value={username}
        onChange={(e) => setUsername(e.target.value)}
        placeholder={"Username"}
        style={styles.input}
      />
      <TextInput
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        placeholder={"Password"}
        secureTextEntry={true}
        style={styles.input}
      />

      <Button
        title={"Login"}
        style={styles.input}
        onPress={() => {
          account.find(
            (usr) => usr.username === username && usr.password === password
          )
            ? alert("Berhasil Login")
            : alert("Salah user/password");
        }}
      />
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    // backgroundColor: "#ecf0f1",
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 10,
  },
});
