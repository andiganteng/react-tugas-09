import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import Login from "./src/Login";
import Register from "./src/Register";

const Stack = createStackNavigator();
// const Tab = createBottomTabNavigator();

export default function App() {
  const [account, setAccount] = useState([
    { username: "andi", password: "123456" },
  ]);

  return (
    <View style={styles.container}>
      {/* <Login account={account} /> */}
      <Register account={account} setAccount={setAccount} />

      {/* <StatusBar style="auto" /> */}
    </View>
    // <NavigationContainer>
    //   <Stack.Navigator>
    //     <Stack.Screen
    //       name="Login"
    //       children={() => <Login account={account} />}
    //     />
    //   </Stack.Navigator>
    // </NavigationContainer>

    // <NavigationContainer>
    //   <Tab.Navigator
    //     initialRouteName="Feed"
    //     tabBarOptions={{
    //       activeTintColor: "#42f44b",
    //     }}
    //   >
    //     <Tab.Screen
    //       name="LoginStack"
    //       children={() => <Login account={account} />}
    //       options={{
    //         tabBarLabel: "Login",
    //         tabBarIcon: ({ color, size }) => (
    //           <MaterialCommunityIcons name="home" color={color} size={size} />
    //         ),
    //       }}
    //     />
    //     <Tab.Screen
    //       name="RegisterStack"
    //       children={() => (
    //         <Register account={account} setAccount={setAccount} />
    //       )}
    //       options={{
    //         tabBarLabel: "Register",
    //         tabBarIcon: ({ color, size }) => (
    //           <MaterialCommunityIcons
    //             name="settings"
    //             color={color}
    //             size={size}
    //           />
    //         ),
    //       }}
    //     />
    //   </Tab.Navigator>
    // </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
